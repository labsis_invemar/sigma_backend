# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class VmEstructura(models.Model):
    id_estructura = models.FloatField(primary_key=True, blank=True, null=False)
    ano = models.FloatField(blank=True, null=True)
    id_departamento = models.TextField(blank=True, null=True)  # This field type is a guess.
    departamento = models.TextField(blank=True, null=True)  # This field type is a guess.
    estacion = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_estacion = models.TextField(blank=True, null=True)  # This field type is a guess.
    municipio = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_municipio = models.IntegerField(blank=True, null=True)
    parcela = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_parcela = models.FloatField(blank=True, null=True)
    especie = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_especie = models.TextField(blank=True, null=True)  # This field type is a guess.
    latitud = models.FloatField(blank=True, null=True)
    longitud = models.FloatField(blank=True, null=True)
    densidad_estacion = models.FloatField(blank=True, null=True)
    densidad_absoluta_especie = models.FloatField(blank=True, null=True)
    abundancia_relativa_especie = models.FloatField(blank=True, null=True)
    area_basal_especie = models.FloatField(blank=True, null=True)
    area_basal_estacion = models.FloatField(blank=True, null=True)
    dominancia_relativa = models.FloatField(blank=True, null=True)
    frecuencia_relativa_especie = models.FloatField(blank=True, null=True)
    indice_valor_importancia = models.FloatField(blank=True, null=True)
    fecha_actualizacion = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'VM_ESTRUCTURA'


class VmCaracterizacion(models.Model):
    id_parcela = models.FloatField(primary_key=True,blank=True, null=False)
    parcela = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_estacion = models.FloatField(blank=True, null=True)
    estacion = models.TextField(blank=True, null=True)  # This field type is a guess.
    tipo = models.FloatField(blank=True, null=True)
    zonaprotegida = models.TextField(blank=True, null=True)  # This field type is a guess.
    sector = models.TextField(blank=True, null=True)  # This field type is a guess.
    region = models.TextField(blank=True, null=True)  # This field type is a guess.
    cuencahidrografica = models.TextField(blank=True, null=True)  # This field type is a guess.
    categoriaunidadmanejo = models.TextField(blank=True, null=True)  # This field type is a guess.
    uac = models.TextField(blank=True, null=True)  # This field type is a guess.
    departamento = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_car = models.TextField(blank=True, null=True)  # This field type is a guess.
    fecha = models.TextField(blank=True, null=True)  # This field type is a guess.
    area = models.FloatField(blank=True, null=True)
    formaparcela = models.FloatField(blank=True, null=True)
    longitud = models.FloatField(blank=True, null=True)
    latitud = models.FloatField(blank=True, null=True)
    tipofisiografico = models.FloatField(blank=True, null=True)
    datosdisponibles = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'VM_CARACTERIZACION'
        
'''class ClstEcorregion(models.Model):
    use_db = 'manglarprueba'
    descripcion = models.CharField(max_length=30)
    codigo_ecorregion = models.CharField(unique=True, max_length=4)
    consecutivo = models.IntegerField(primary_key=True)
    uac = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'CLST_ECORREGION' 
        
class V2021DatosSiembra(models.Model):
    use_db = 'manglar'
    objectid = models.IntegerField(primary_key=True, blank=True, null=True)
    globalid = models.IntegerField(blank=True, null=True)
    sect_perso = models.CharField(max_length=8, blank=True, null=True)
    orga = models.CharField(max_length=4000, blank=True, null=True)
    aliad = models.CharField(max_length=15, blank=True, null=True)
    nomb_pers_org = models.CharField(max_length=70, blank=True, null=True)
    apell = models.CharField(max_length=70)
    id_pers = models.IntegerField()
    edad = models.CharField(blank=True, null=True)
    corr_pers = models.CharField(max_length=100)
    mindate = models.DateField(blank=True, null=True)
    maxdate = models.DateField(blank=True, null=True)
    fec_siemb = models.IntegerField(blank=True, null=True)
    dpto = models.CharField(max_length=100)
    mpio = models.CharField(max_length=100)
    cp = models.CharField(max_length=300)
    esp_plant = models.CharField(max_length=4000, blank=True, null=True)
    cant_arb = models.IntegerField(blank=True, null=True)
    latitud = models.FloatField(blank=True, null=True)
    longitud = models.FloatField(blank=True, null=True)
    obsrv = models.CharField(blank=True, null=True)
    un = models.CharField(max_length=16, blank=True, null=True)
    usua = models.CharField(max_length=15, blank=True, null=True)
    valid = models.CharField(max_length=3, blank=True, null=True)
    tst = models.CharField(blank=True, null=True)
    wp = models.CharField(blank=True, null=True)
    noteeee = models.CharField(blank=True, null=True)
    url = models.CharField(blank=True, null=True)
    term = models.CharField(max_length=2, blank=True, null=True)
    term_connd = models.CharField(blank=True, null=True)
    creationdate = models.IntegerField(blank=True, null=True)
    creator = models.CharField(max_length=16, blank=True, null=True)
    editdate = models.IntegerField(blank=True, null=True)
    editor = models.CharField(max_length=16, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'v_2021_datos_siembra' '''
