from django.contrib import admin
from django.urls import path, re_path
from .views import *


urlpatterns = [
    path(r'api/estructura/data-estructura/', EstrData.as_view(),name='Estructura'),
    path(r'api/estructura/departamentos',DepList.as_view(),name='estrutura_api_dep'),
    path(r'api/estructura/municipios/<id_dep>/',MunList.as_view(),name='estrutura_api_mun'),
    path(r'api/estructura/estaciones/<id_mun>/',EstList.as_view(),name='estacion_api_all'),
    path(r'api/estructura/parcelas/<id_est>/',ParList.as_view(),name='parcelas_api_all'),
    path(r'api/estructura/especies/<id_par>/',EspecieList.as_view(),name='ano_api_all'),
    path(r'api/estructura/consulta-variables/<id_parc>/<id_esp>/<variable>/',EstructuraEspecie.as_view(),name='est_esp_var'), #api de datos a pintar
    path(r'api/estructura/caracterizacion',CaracterizacionList.as_view(),name='caracterizacion_all'),
    path(r'api/estructura/caract-gis',CaracterizacionListGis.as_view(),name='caract-gis'),
    path(r'api/estructura/caract-gis?format=json',CaracterizacionListGis.as_view(),name='caract-gisJson'),
    #path(r'api/ecorregiones',Ecorregiones.as_view(),name='ecorregiones'),
    #path(r'api/contadorArboles',DatosSiembra.as_view(),name='DatosSiembra'),
]
