from django.db.models.query import QuerySet
from django.shortcuts import render
from django.views.generic import TemplateView
from .models import *
from .serializers import *
from rest_framework.generics import(ListAPIView,)
from rest_framework.views import APIView
from rest_framework.response import Response
import requests


# Create your views here.

#-----------Prueba---------------
class EstrData(ListAPIView):
    serializer_class = EstructuraGisSerializer
    
    def get_queryset(self):
        queryset = VmEstructura.objects.all()
        return queryset

#--------------------------------




#-----------Datos vm_estructura-------------

#Datos filtrados parcela
class EstructuraParList(ListAPIView):
    serializer_class = VmEstructuraSerializer
    
    def get_queryset(self):
        par = self.kwargs['id_par']  
        queryset = VmEstructura.objects.filter(id_parcela=par).order_by('parcela')
        return queryset



#---------------Lista de seleccionables--------------

#Lista de Departamento
class DepList(ListAPIView):
    queryset = VmEstructura.objects.values('id_departamento','departamento').filter(departamento__isnull=False).distinct().order_by('departamento')
    serializer_class = DepartEstSerializer

class MunList(ListAPIView):
    serializer_class = MunicipioEstSerializer
    
    def get_queryset(self):
        dep = self.kwargs['id_dep']
        queryset = VmEstructura.objects.values('id_municipio','municipio').distinct().filter(id_departamento=dep).order_by('municipio')
        return queryset

#lista de estaciones
class EstList(ListAPIView):
    serializer_class = EstacionEstSerializer
    
    def get_queryset(self):
        mun = self.kwargs['id_mun']
        queryset = VmEstructura.objects.values('id_estacion','estacion').distinct().filter(id_municipio=mun).order_by('estacion')
        return queryset
    
    

#Lista de parcelas segun la estacion
class ParList(ListAPIView):
    serializer_class = ParcelaEstSerializer

    def get_queryset(self):
        est = self.kwargs['id_est']
        queryset = VmEstructura.objects.values('id_estacion','parcela','id_parcela').distinct().filter(id_estacion=est).order_by('parcela')
        return queryset


#Lista de especie segun la parcela

class EspecieList(ListAPIView):
    serializer_class = EspecieEstSerializer

    def get_queryset(self):
        par = self.kwargs['id_par']
        queryset = VmEstructura.objects.values('id_especie','especie').distinct().filter(id_parcela=par).order_by('especie')
        return queryset

#-----------------------------------------------------


#--------------------Datos de estructura segun especie y variable-------------------------------
# class EstructuraEspecie(ListAPIView):
#     serializer_class = VmEstructuraSerializer

#     def get_queryset(self):
#         par = self.kwargs['id_parc']
#         esp = self.kwargs['id_esp']
#         var = self.kwargs['variable']
#         if esp == "total":
#             queryset =  VmEstructura.objects.values('ano','especie',var).filter(id_parcela=par).order_by('ano','especie')
#         else:
#             queryset =  VmEstructura.objects.values('ano','especie',var).filter(id_parcela=par).filter(id_especie=esp).order_by('ano','especie')
#         return queryset
#-----------------------------------------------------------------------------------------------

class EstructuraEspecie(APIView):

    def get(self,request,id_parc=None, id_esp=None, variable=None):
        if id_esp == "total":
            datos =  VmEstructura.objects.filter(id_parcela=id_parc).order_by('ano','especie').values('ano','especie',variable)
        else:
            datos =  VmEstructura.objects.filter(id_parcela=id_parc).filter(id_especie=id_esp).order_by('ano','especie').values('ano','especie',variable)
        
        serializer= VmEstructuraSerializer(datos, many=True,partial=True)
        
        array=[]
        for x in serializer.data:
            dict1={}
            for y in x:
                print(y)
                if x[y] is not None:
                    dict1[y]=x[y]
            array.append(dict1)     
        return Response(array)
#-----------------------------------------------------------------------------------------------



#--------------Datos de datos Caracterizacion-----------------
class CaracterizacionList(ListAPIView):
    serializer_class = CaracterizacionSerializer

    def get_queryset(self):
        queryset = VmCaracterizacion.objects.all()
        return queryset
#--------------------------------------------------------------


class CaracterizacionListGis(ListAPIView):
    serializer_class = CaracterizacionGisSerializer

    def get_queryset(self):
        queryset = VmCaracterizacion.objects.all()
        return queryset

'''class Ecorregiones(APIView):
    #lista los daatos de siembra
    def get(self, request, format=None):
        datos = ClstEcorregion.objects.using('geografico').all()
        serializer = ecorregionSerializer(datos, many=True)
        return Response(serializer.data)
class DatosSiembra(APIView):
    #lista los daatos de siembra
    def get(self, request, format=None):
        datos = V2021DatosSiembra.objects.using('manglar').all()
        serializer = ecorregionSerializer(datos, many=True)
        return Response(serializer.data)'''
