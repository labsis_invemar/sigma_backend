from django.db.models import fields
from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer,GeometrySerializerMethodField
from django.contrib.gis.geos import Point
from .models import *


class VmEstructuraSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmEstructura
        fields =('__all__')

    

#--------------------------------------


#-----------Vista de valores a listar---------------
class DepartEstSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmEstructura
        fields = ('id_departamento','departamento')

class MunicipioEstSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmEstructura
        fields = ('municipio','id_municipio')

class EstacionEstSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmEstructura
        fields = ('estacion','id_estacion')

class ParcelaEstSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmEstructura
        fields = ('id_estacion','parcela','id_parcela')


class EspecieEstSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmEstructura
        fields = ('id_especie','especie')

#------------------------------------------------------------------------------------

#-------------------Datos de estructura segun especie y variables---------------------
class EstructuraDatosEspecie (serializers.ModelSerializer):
    class Meta:
        model = VmEstructura
        fields = ('__all__')
#-----------------------------------------------------------------------------------




class CaracterizacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmCaracterizacion
        fields =('id_parcela','parcela','id_estacion','estacion','tipo','zonaprotegida','sector','region','cuencahidrografica','categoriaunidadmanejo','uac','departamento','id_car','fecha','area','formaparcela','longitud','latitud','tipofisiografico','datosdisponibles')

class CaracterizacionGisSerializer(GeoFeatureModelSerializer):
    pnt = GeometrySerializerMethodField()    
    class Meta:
        model = VmCaracterizacion
        geo_field = 'pnt'
        fields=('id_parcela','parcela','id_estacion','estacion','tipo','zonaprotegida','sector','region','cuencahidrografica','categoriaunidadmanejo','uac','departamento','id_car','fecha','area','formaparcela','longitud','latitud','tipofisiografico','datosdisponibles','pnt')
    
    def get_pnt(self, obj):
        return Point(float(obj.longitud),float(obj.latitud))




class EstructuraGisSerializer(GeoFeatureModelSerializer):
    pnt = GeometrySerializerMethodField()
    class Meta:
        model = VmEstructura
        geo_field = 'pnt'
        fields=('__all__')
    
    def get_pnt(self, obj):
       return Point(float(obj.longitud),float(obj.latitud))

#'ano','id_departamento','departamento','id_estacion','estacion','id_parcela','parcela','id_especie','especie','latitud','longitud','densidad_estacion','densidad_absoluta_especie','abundancia_relativa_especie','area_basal_especie','area_basal_estacion','dominancia_relativa','frecuencia_relativa_especie','indice_valor_importancia','pnt'


'''#-------------------Datos de ecorregiones---------------------
class ecorregionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClstEcorregion
        fields = ('__all__')
#----------------------------------------------------------------------------------- 

#-------------------Datos de siembra---------------------
class siembraSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021DatosSiembra
        fields = ('__all__')
#-----------------------------------------------------------------------------------'''
