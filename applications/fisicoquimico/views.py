from django.shortcuts import render
from django.views.generic import TemplateView
from .models import *
from .serializers import *
from rest_framework.generics import(ListAPIView,)
from rest_framework.views import APIView
from rest_framework.response import Response
import requests
# Create your views here.

#-----------Datos vm_fisicoquimico-------------

#Filtrados por Estacion
class FisicoQuimicoList(APIView):

    def get(self, request, id_est=None, var=None):
        datos = VmFisicoquimico.objects.filter(id_estacion=id_est).order_by('ano').values('ano',var)
        
        serializer=VmFisicoQuimicoSerializer(datos,many=True,partial=True)

        array=[]
        for x in serializer.data:
            dict1={}
            for y in x:
                print(y)
                if x[y] is not None:
                    dict1[y]=x[y]
            array.append(dict1)    
        return Response(array)

#Datos filtrados solo por departamento
class FisicoQuimicoDepList(ListAPIView):
    serializer_class = VmFisicoQuimicoSerializer
    
    def get_queryset(self):
        dep = self.kwargs['id_dep']
        queryset = VmFisicoquimico.objects.filter(id_departamento=dep).order_by('ano')
        return queryset


#---------------Lista de seleccionables--------------
#Lista de departamentos
class DepList(ListAPIView):
    queryset = VmFisicoquimico.objects.values('id_departamento','departamento').filter(departamento__isnull=False).distinct().order_by('departamento')
    serializer_class = DepartamentoFqSerializer

#Lista de municipios segun el departamento
class MunList(ListAPIView):
    serializer_class = MunicipioFqSerializer

    def get_queryset(self):
        dep = self.kwargs['id_dep']
        queryset = VmFisicoquimico.objects.values('id_municipio','municipio').distinct().filter(id_departamento=dep).order_by('municipio')
        return queryset

#-----------------------------------------------------


#Lista de estaciones segun el departamento
class EstList(ListAPIView):
    serializer_class = EstacionFqSerializer

    def get_queryset(self):
        mun = self.kwargs['id_mun']
        queryset = VmFisicoquimico.objects.values('id_departamento','id_estacion','estacion',).distinct().filter(id_municipio=mun).order_by('estacion')
        return queryset

#-----------------------------------------------------



