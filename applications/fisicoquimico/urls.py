from django.contrib import admin
from django.urls import path
from .views import *

    
urlpatterns = [
    path(r'api/fisicoquimico/data-departamentos/<id_dep>/',FisicoQuimicoDepList.as_view(),name='fisicoquimico_api_departamento'),
    path(r'api/fisicoquimico/data-estaciones/<id_est>/<var>/',FisicoQuimicoList.as_view(),name='fisicoquimico_api_all'),
    path(r'api/fisicoquimico/departamentos',DepList.as_view(),name='departamentosfq_api_all'),
    path(r'api/fisicoquimico/municipios/<id_dep>/',MunList.as_view(),name='municipiosfq_api_all'),
    path(r'api/fisicoquimico/estaciones/<id_mun>/',EstList.as_view(),name='estacionesfq_api_all'),
    
]
