from django.apps import AppConfig


class FisicoquimicoConfig(AppConfig):
    name = 'fisicoquimico'
