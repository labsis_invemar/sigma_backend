from rest_framework import serializers
from .models import *


class VmFisicoQuimicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmFisicoquimico
        fields =('__all__')
class DepartamentoFqSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmFisicoquimico
        fields = ('id_departamento','departamento')

class MunicipioFqSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmFisicoquimico
        fields = ('id_municipio','municipio')


class EstacionFqSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmFisicoquimico
        fields = ('id_estacion','estacion')

