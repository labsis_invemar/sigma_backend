# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class VmFisicoquimico(models.Model):
    ano = models.FloatField(primary_key=True,blank=True, null=False)
    region = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_region = models.TextField(blank=True, null=True)  # This field type is a guess.
    uac = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_uac = models.TextField(blank=True, null=True)  # This field type is a guess.
    departamento = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_departamento = models.TextField(blank=True, null=True)  # This field type is a guess.
    municipio = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_municipio = models.IntegerField(blank=True, null=True)
    sector = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_sector = models.TextField(blank=True, null=True)  # This field type is a guess.
    unidad_de_manejo = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_unidad_de_manejo = models.TextField(blank=True, null=True)  # This field type is a guess.
    estacion = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_estacion = models.TextField(blank=True, null=True)  # This field type is a guess.
    pro_nivel_agua = models.FloatField(blank=True, null=True)
    pro_sal_1 = models.FloatField(blank=True, null=True)
    pro_sal_05 = models.FloatField(blank=True, null=True)
    pro_sal_sup = models.FloatField(blank=True, null=True)
    pro_tem_sup = models.FloatField(blank=True, null=True)
    pro_tem_1 = models.FloatField(blank=True, null=True)
    pro_tem_05 = models.FloatField(blank=True, null=True)
    pro_ph_sub = models.FloatField(blank=True, null=True)
    pro_ph_1 = models.FloatField(blank=True, null=True)
    pro_ph_05 = models.FloatField(blank=True, null=True)
    pro_con_sup = models.FloatField(blank=True, null=True)
    pro_con_1 = models.FloatField(blank=True, null=True)
    pro_con_05 = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'vm_fisicoquimico'
