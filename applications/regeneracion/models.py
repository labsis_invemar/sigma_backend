from django.db import models

class AgdMuestras(models.Model):
    id_muestra = models.FloatField(primary_key=True)
    id_muestreo = models.ForeignKey('AgdMuestreos', models.DO_NOTHING, db_column='id_muestreo')
    notas = models.CharField(max_length=1000, blank=True, null=True)
    es_replica = models.FloatField(blank=True, null=True)
    fechasis = models.DateField(blank=True, null=True)
    id_muestratex = models.CharField(max_length=40)
    id_muestreotex = models.CharField(max_length=40)

    class Meta:
        managed = False
        db_table = 'agd_muestras'
        
class AgdMuestrasVariables(models.Model):
    id_parametro = models.ForeignKey('AgmCampo', models.DO_NOTHING, db_column='id_parametro', primary_key=True)
    id_metodologia = models.ForeignKey('AgmCampo', models.DO_NOTHING, db_column='id_metodologia')
    id_unidad_medida = models.ForeignKey('AgmCampo', models.DO_NOTHING, db_column='id_unidad_medida')
    id_muestra = models.ForeignKey(AgdMuestras, models.DO_NOTHING, db_column='id_muestra')
    id_metodo = models.ForeignKey('AgmMetodos', models.DO_NOTHING, db_column='id_metodo', blank=True, null=True)
    valor = models.CharField(max_length=2000, blank=True, null=True)
    quality_flag = models.FloatField(blank=True, null=True)
    precision = models.FloatField(blank=True, null=True)
    fechasis = models.DateField(blank=True, null=True)
    parametro_unidades = models.CharField(max_length=200)
    id_muestratex = models.CharField(max_length=40)
    aud_update = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'agd_muestras_variables'
        unique_together = (('id_parametro', 'id_metodologia', 'id_muestra', 'id_unidad_medida'),)


class AgdMuestreos(models.Model):
    id_muestreo = models.FloatField(primary_key=True)
    id_estacion = models.FloatField()
    id_proyecto = models.ForeignKey('AgmMetodologiaxproyecto', models.DO_NOTHING, db_column='id_proyecto')
    id_metodologia = models.ForeignKey('AgmMetodologias', models.DO_NOTHING, db_column='id_metodologia')
    id_tematicas = models.ForeignKey('AgmTematica', models.DO_NOTHING, db_column='id_tematicas')
    fecha = models.DateField()
    notas = models.CharField(max_length=1000, blank=True, null=True)
    fechasis = models.DateField(blank=True, null=True)
    id_muestreotex = models.CharField(max_length=40)

    class Meta:
        managed = False
        db_table = 'agd_muestreos'


class AgdMuestreosParametros(models.Model):
    id_muestreo = models.ForeignKey(AgdMuestreos, models.DO_NOTHING, db_column='id_muestreo', primary_key=True)
    id_parametro = models.ForeignKey('AgmCampo', models.DO_NOTHING, db_column='id_parametro')
    id_metodologia = models.ForeignKey('AgmCampo', models.DO_NOTHING, db_column='id_metodologia')
    id_unidad_medida = models.ForeignKey('AgmCampo', models.DO_NOTHING, db_column='id_unidad_medida')
    valor = models.CharField(max_length=2000, blank=True, null=True)
    fechasis = models.DateField(blank=True, null=True)
    id_muestreotex = models.CharField(max_length=40)

    class Meta:
        managed = False
        db_table = 'agd_muestreos_parametros'
        unique_together = (('id_muestreo', 'id_parametro', 'id_metodologia', 'id_unidad_medida'),)

class VmRegeneracion(models.Model):
    id = models.IntegerField(blank=True, null=True, db_column = 'id_regeneracion', primary_key = True)
    ano = models.IntegerField(blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    id_departamento = models.CharField(max_length=3000, blank=True, null=True)
    departamento = models.CharField(max_length=100, blank=True, null=True)
    id_municipio = models.IntegerField(blank=True, null=True)
    municipio = models.TextField(blank=True, null=True)  # This field type is a guess.
    estacion = models.CharField(max_length=4000, blank=True, null=True)
    id_estacion = models.CharField(max_length=4000, blank=True, null=True)
    parcela = models.CharField(max_length=4000, blank=True, null=True)
    id_parcela = models.FloatField(blank=True, null=True)
    especie = models.CharField(max_length=4000, blank=True, null=True)
    id_especie = models.CharField(max_length=2000, blank=True, null=True)
    
    area = models.IntegerField(blank=True, null=True)
    
    n_prop = models.IntegerField(blank=True, null=True)
    n_plant = models.IntegerField(blank=True, null=True)
    # n_propxesp = models.IntegerField(blank=True, null=True)
    # n_plantxesp = models.IntegerField(blank=True, null=True)
    densidad_prop = models.FloatField(blank=True, null=True)   
    densidad_plant = models.FloatField(blank=True, null=True)
    # dens_propxesp = models.FloatField(blank=True, null=True)
    # dens_plantxesp = models.FloatField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'vm_regeneracion'
        
        
        
