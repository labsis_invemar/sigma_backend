from django.contrib import admin
from django.urls import path
from . import views
from .views import *

    
urlpatterns = [
    path(r'api/regeneracion/data-anio/',RegeneracionList.as_view(),name='get-regeneracion'),#data filtrada por año
    path(r'api/regeneracion/departamentos/',DepartamentoRegeneracionList.as_view(),name='get-regeneracion-departamento'),
    path(r'api/regeneracion/municipios/<id_dep>/',MunicipioRegeneracionList.as_view(),name='get-regeneracion-municipio'),
    path(r'api/regeneracion/estaciones/<id_munic>/',EstacionRegeneracionList.as_view(),name='get-regeneracion-estacion'),
    path(r'api/regeneracion/parcelas/<id_est>/',ParcelaRegeneracionList.as_view(),name='regeneracion-parcela'),
    path(r'api/regeneracion/especies/<id_parc>/',EspecieRegeneracionList.as_view(),name='regeneracion-especie'),
    path(r'api/regeneracion/anios/<id_parc>/<var>/',Anios_Regeneracion.as_view(),name='Anios_Regeneracion'),
    path(r'api/regeneracion/consulta-variables/<id_anio>/<id_parc>/<var>/',RegeneracionAllList.as_view(),name='regeneracion-all'),
    
    
    path(r'api/regeneracion/dens-prop-est-regeneracion/<id_esp>/<id_parc>/<ano>/',DensPropEst.as_view(),name='get-dens-prop-est-regeneracion'),
    
]
