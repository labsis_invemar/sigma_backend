from django.db.models import query
from django.shortcuts import render
from django.shortcuts import render
from django.views.generic import TemplateView
from .models import *
from .serializers import *
from rest_framework.generics import(ListAPIView,)
from rest_framework.views import APIView
from rest_framework.response import Response
import requests

# Create your views here.

class RegeneracionList(ListAPIView):
    serializer_class = VmRegeneracionSerializer

    def get_queryset(self):
        queryset = VmRegeneracion.objects.all().order_by('ano')
        return queryset


#-----Listado de departamento--------
class DepartamentoRegeneracionList(ListAPIView):
    queryset = VmRegeneracion.objects.filter(departamento__isnull=False).values('id_departamento','departamento').distinct().order_by('departamento')
    serializer_class = VmRegeneracionDepartamentoSerializer

#-----Listado de municipios-----------------------------
class MunicipioRegeneracionList(ListAPIView):
    serializer_class = VmRegeneracionMunicipioSerializer
    def get_queryset(self):
        dep = self.kwargs['id_dep']
        queryset = VmRegeneracion.objects.filter(estacion__isnull=False).filter(id_departamento=dep).values('id_municipio','municipio').distinct().order_by('municipio')
        return queryset
    
#-----Listado de estación -----------------------------
class EstacionRegeneracionList(ListAPIView):
    serializer_class = VmRegeneracionEstacionSerializer
    
    def get_queryset(self):
        munc = self.kwargs['id_munic']
        queryset = VmRegeneracion.objects.filter(estacion__isnull=False).filter(id_municipio=munc).values('id_estacion','estacion').distinct().order_by('estacion')
        print(queryset)
        return queryset


#------Listado de parcelas-------------------------------

class ParcelaRegeneracionList(ListAPIView):
    serializer_class = VmRegeneracionParcelasSerializer
    
    def get_queryset(self):
        est = self.kwargs['id_est']
        queryset = VmRegeneracion.objects.filter(parcela__isnull=False).filter(id_estacion=est).order_by('parcela').values('id_parcela','parcela').distinct()
        return queryset


#------Listado de especie-------------

class EspecieRegeneracionList(ListAPIView):
    serializer_class = VmRegeneracionEspecieSerializer
    
    def get_queryset(self):
        par = self.kwargs['id_parc']
        queryset = VmRegeneracion.objects.values('id_especie','especie').filter(id_parcela=par).filter(especie__isnull=False).distinct().order_by('especie')
        return queryset

#------Listado de años-------------
class Anios_Regeneracion(ListAPIView):
    serializer_class= Anios_Regeneracion_Serializer
    
    def get_queryset(self):
        id_parc = self.kwargs['id_parc']
        
        datos =  VmRegeneracion.objects.using('manglar').values('ano').filter(ano__isnull=False).filter(id_parcela=id_parc).distinct().order_by('ano')
    
        return datos 
#------------Datos Reneracion---------------
class RegeneracionAllList(APIView):

    def get(self, request, id_anio=None, id_parc=None, var=None):
        if(var == 'densidad_prop' or var == 'densidad_plant'):
            # datos = VmRegeneracion.objects.filter(id_parcela=id_parc).filter(ano=id_anio).order_by('fecha').values('fecha',var,'parcela').distinct()
            datos = VmRegeneracion.objects.filter(id_parcela=id_parc).filter(ano=id_anio).values('ano',var,'parcela').distinct()
            serializer=VmRegeneracionSerializer(datos,many=True,partial=True)
            
        elif(var == 'dens_propxesp' or var == 'dens_plantxesp'):
            datos = VmRegeneracion.objects.filter(ano=id_anio).filter(id_parcela=id_parc).order_by('fecha').values('fecha','especie',var,'parcela').distinct()
            serializer=VmRegeneracionSerializer(datos,many=True,partial=True)

        array=[]
        for x in serializer.data:
            dict1={}
            for y in x:
                print(y)
                if x[y] is not None:
                    dict1[y]=x[y]
            array.append(dict1)    
        return Response(array)

#--------------DensidadxPropagulosxEstacion------------------------
class DensPropEst(ListAPIView):
    serializer_class= DensPropEstSerializer
    
    def get_queryset(self):
        id_parc = self.kwargs['id_parc']    
        esp = self.kwargs['id_esp']
        anio = self.kwargs['ano']
       #select DISTINCT cod_especie, dens_plantulas from v_2021_restauracion where id_estacion = 52736 and cod_especie = 'AG' and ano = 2019
        datos =  VmRegeneracion.objects.using('manglar').filter(id_parcela=id_parc).filter(ano=anio).filter(id_especie=esp).values('fecha','especie','dens_prop').distinct().order_by('fecha')
       
        return datos
#--------------DensidadxPropagulosxEspecie------------------------

#--------------DensidadxPlantulasxEstacion------------------------
#--------------DensidadxPlantulasxEspecie------------------------



