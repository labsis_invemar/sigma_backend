from rest_framework import serializers
from . models import *



class VmRegeneracionSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmRegeneracion
        fields =('__all__')

class VmRegeneracionDepartamentoSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmRegeneracion
        fields =('id_departamento','departamento')
        
class VmRegeneracionMunicipioSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmRegeneracion
        fields =('id_municipio','municipio')


class VmRegeneracionEstacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmRegeneracion
        fields =('id_estacion','estacion')

class VmRegeneracionParcelasSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmRegeneracion
        fields =('id_parcela','parcela')

class VmRegeneracionEspecieSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmRegeneracion
        fields =('id_especie','especie')
        

class DensPropEstSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmRegeneracion
        fields =('fecha','especie','dens_prop')

class VmRegeneracionEspSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmRegeneracion
        fields =('fecha','especie','dens_prop')

class VmRegeneracionParSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmRegeneracion
        fields =('fecha','dens_prop')
        
class Anios_Regeneracion_Serializer(serializers.ModelSerializer):
    class Meta:
        model = VmRegeneracion
        fields =(['ano'])
