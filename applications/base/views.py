from django.db.models.query import QuerySet
from django.shortcuts import render
from django.views.generic import TemplateView
from .models import *
from .serializers import *
from rest_framework.generics import(ListAPIView,)
from rest_framework.views import APIView
from rest_framework.response import Response
import requests



#----------------Lista de seleccionable------------

#------------Departamento-------------
class DepIbimList(ListAPIView):
    queryset = VmIbim.objects.values('id_departamento','departamento').distinct()
    serializer_class = DepartamentoIbimSerializer


#-------------Estacion----------------
class EstIbimList(ListAPIView):
    serializer_class = EstacionIbimSerializer
    
    def get_queryset(self):
        dep = self.kwargs['id_dep']
        queryset = VmIbim.objects.values('id_estacion','estacion').distinct().filter(id_departamento=dep).order_by('estacion')
        return queryset



#-------------DatosIbim------------------

class IbimList(ListAPIView):
    serializer_class = VmIbimSerializer
    
    def get_queryset(self):
        est = self.kwargs['id_est']
        queryset = VmIbim.objects.filter(id_estacion=est).order_by('ano')
        return queryset

