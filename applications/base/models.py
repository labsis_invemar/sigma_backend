from django.db import models

class VmIbim(models.Model):
    ano = models.FloatField(primary_key=True, blank=True, null=False)
    region = models.TextField(blank=True, null=True)  # This field type is a guess.
    uac = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_departamento = models.TextField(blank=True, null=True)  # This field type is a guess.
    departamento = models.TextField(blank=True, null=True)  # This field type is a guess.
    sector = models.TextField(blank=True, null=True)  # This field type is a guess.
    unidad_de_manejo = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_estacion = models.TextField(blank=True, null=True)  # This field type is a guess.
    estacion = models.TextField(blank=True, null=True)  # This field type is a guess.
    ab_rm = models.FloatField(blank=True, null=True)
    den_rm = models.FloatField(blank=True, null=True)
    ab_lr = models.FloatField(blank=True, null=True)
    den_lr = models.FloatField(blank=True, null=True)
    ab_ag = models.FloatField(blank=True, null=True)
    den_ag = models.FloatField(blank=True, null=True)
    sal_0_5_m_prom = models.FloatField(blank=True, null=True)
    plantulas = models.FloatField(blank=True, null=True)
    n_propagulos = models.FloatField(blank=True, null=True)
    calf_ab_ag = models.FloatField(blank=True, null=True)
    calf_ab_lr = models.FloatField(blank=True, null=True)
    calf_ab_rm = models.FloatField(blank=True, null=True)
    calf_den_ag = models.FloatField(blank=True, null=True)
    calf_den_lr = models.FloatField(blank=True, null=True)
    calf_den_rm = models.FloatField(blank=True, null=True)
    calf_sal = models.FloatField(blank=True, null=True)
    calf_pla = models.FloatField(blank=True, null=True)
    calf_pro = models.FloatField(blank=True, null=True)
    ibi = models.FloatField(blank=True, null=True)
    calificacion = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'VM_IBIM'
