from django.db.models import fields
from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer,GeometrySerializerMethodField
from django.contrib.gis.geos import Point
from .models import *

#-------------Total de datos-----------------
class VmIbimSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmIbim
        fields =('ano','ibi','calificacion')



#-------------valores de listas-----------------
class DepartamentoIbimSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmIbim
        fields = ('id_departamento','departamento')


class EstacionIbimSerializer(serializers.ModelSerializer):
    class Meta:
        model = VmIbim
        fields = ('id_estacion','estacion')