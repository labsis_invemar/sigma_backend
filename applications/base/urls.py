from django.contrib import admin
from django.urls import path, re_path
from .views import *


urlpatterns = [
    path(r'api/get-departamento-ibim/', DepIbimList.as_view(),name='departamento_ibim'),
    path(r'api/get-estacion-ibim/<id_dep>/',EstIbimList.as_view(),name='estrutura_Ibim'),
    path(r'api/get-ibim/<id_est>/',IbimList.as_view(),name='Ibim_all'),
]

