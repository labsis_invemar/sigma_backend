
from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import V2021DatosSiembra 
from .models import V2021Restauracion
from .models import V_tasas_restauracion
from applications.estructura.models import *

#-------------------Datos de para la siembra de arboles---------------------
class siembraSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021DatosSiembra
        fields = ('__all__')
#-----------------------------------------------------------------------------------
# class DepRestSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = V2021Restauracion
#         fields = ('cod_departamento','departamento')
        
# class DensxDepSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = VmDensidadDep
#         fields = ('name','y')
        
class DepListRestauracionSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields = ('cod_departamento','departamento')
        
class municipio_list_serializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields = ('id_municipio','municipio')

class restauracionDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields = ('__all__')
class ProjListSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields = ('id_proyecto','proyecto')
class EstacionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields = ('id_estacion','estacion')
class EspecieListSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields = ('cod_especie','especie')

class ParcelaListSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields = ('id_parcela','parcela')
        
class VmRestauracionSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields =('especie','siembraxesp')
        
class SiembraxDepartamentosSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields =('departamento','porc_siembraxdep')
        
class DataGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        # fields =('__all__')
        fields =('id_proyecto','proyecto','id_estacion','estacion','siembraxest','ESPXEST','ano','N_MONITOREOS','FECHA_INICIAL_MONITOREOS','ENTIDAD_FINANCIADORA','PLAZO_EJECUCION','FUENTE_FINANCIACION','AREA')
        
        
class AniosRestauracionSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        # fields =('id_restauracion','ano')
        fields =(['ano'])

class MortalidadxEspSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields =('especie','TASA_MORTALIDAD')
class SupervivenciaxEspSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields =('especie','TASA_SUPERVIVENCIA')        
class DensPropSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields =('especie','dens_propagulos')
        
class DensPlantSerializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields =('especie','dens_plantulas')
        
class TasaMortalidadRestauracion(serializers.ModelSerializer):
    class Meta:
        model = V_tasas_restauracion
        fields =('FECHA','ESPECIE_DES','TASA_MORTALIDAD')
        
class TasaSupervivenciaRestauracion(serializers.ModelSerializer):
    class Meta:
        model = V_tasas_restauracion
        fields =('FECHA','ESPECIE_DES','TASA_SUPERVIVENCIA')
        
class area_reportada_serializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields =('area')

class areas_restauracion_serializer(serializers.ModelSerializer):
    class Meta:
        model = V2021Restauracion
        fields =('id_estacion','estacion','AREA')