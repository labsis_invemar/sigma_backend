from django.db import models

# Create your models here.    
class V2021DatosSiembra(models.Model):
    use_db = 'manglar'
    objectid = models.IntegerField(primary_key=True, blank=True, null=True)
    globalid = models.IntegerField(blank=True, null=True)
    sect_perso = models.CharField(max_length=8, blank=True, null=True)
    orga = models.CharField(max_length=4000, blank=True, null=True)
    aliad = models.CharField(max_length=15, blank=True, null=True)
    nomb_pers_org = models.CharField(max_length=70, blank=True, null=True)
    apell = models.CharField(max_length=70)
    id_pers = models.IntegerField()
    edad = models.CharField(blank=True, null=True)
    corr_pers = models.CharField(max_length=100)
    mindate = models.DateField(blank=True, null=True)
    maxdate = models.DateField(blank=True, null=True)
    fec_siemb = models.IntegerField(blank=True, null=True)
    dpto = models.CharField(max_length=100)
    mpio = models.CharField(max_length=100)
    cp_id = models.IntegerField(blank=True, null=True)
    cp = models.CharField(max_length=300)
    esp_plant = models.CharField(max_length=4000, blank=True, null=True)
    cant_arb = models.IntegerField(blank=True, null=True)
    latitud = models.FloatField(blank=True, null=True)
    longitud = models.FloatField(blank=True, null=True)
    obsrv = models.CharField(blank=True, null=True)
    un = models.CharField(max_length=16, blank=True, null=True)
    usua = models.CharField(max_length=15, blank=True, null=True)
    valid = models.CharField(max_length=3, blank=True, null=True)
    tst = models.CharField(blank=True, null=True)
    wp = models.CharField(blank=True, null=True)
    noteeee = models.CharField(blank=True, null=True)
    url = models.CharField(blank=True, null=True)
    term = models.CharField(max_length=2, blank=True, null=True)
    term_connd = models.CharField(blank=True, null=True)
    creationdate = models.IntegerField(blank=True, null=True)
    creator = models.CharField(max_length=16, blank=True, null=True)
    editdate = models.IntegerField(blank=True, null=True)
    editor = models.CharField(max_length=16, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'v_2021_datos_siembra'
        


class V2021Restauracion(models.Model): 
    use_db = 'manglar'
    id_restauracion = models.IntegerField(primary_key=True, blank=True, null=False)
    ano = models.IntegerField(blank=True, null=True) 
    fecha= models.TextField(blank=True, null=True) 
    cod_departamento = models.CharField(blank=True, null=True)
    departamento = models.CharField(max_length=100)
    id_municipio = models.IntegerField(blank=True, null=True)
    municipio = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_proyecto = models.CharField(blank=True, null=True)
    proyecto = models.CharField(max_length=2000, blank=True, null=True)
    id_estacion = models.IntegerField()
    estacion = models.CharField(max_length=4000, blank=True, null=True)
    #id_parcela = models.CharField(blank=True, null=True)
    #parcela = models.CharField(blank=True, null=True)
    cod_especie = models.CharField(max_length=2000, blank=True, null=True)
    especie = models.CharField(max_length=4000, blank=True, null=True)
    siembraxest = models.IntegerField()   
    ESPXEST = models.CharField(max_length=2000, blank=True, null=True)
    siembraxesp = models.IntegerField(blank=True, null=True)   
    porc_siembraxdep = models.FloatField(blank=True, null=True)
    dens_plantulas = models.FloatField(blank=True, null=True)   
    dens_propagulos = models.FloatField(blank=True, null=True)
    N_MONITOREOS = models.IntegerField(blank=True, null=True)
    FECHA_INICIAL_MONITOREOS = models.CharField(blank=True, null=True)
    ENTIDAD_FINANCIADORA = models.CharField(blank=True, null=True)
    PLAZO_EJECUCION = models.IntegerField(blank=True, null=True)
    FUENTE_FINANCIACION = models.CharField(blank=True, null=True)
    AREA = models.FloatField(blank=True, null=True)
    # TASA_MORTALIDAD = models.FloatField()   
    # TASA_SUPERVIVENCIA = models.FloatField()   
    class Meta:
        managed = False
        db_table = 'vm_restauracion'
        
class V_tasas_restauracion(models.Model): 
    use_db = 'manglar'
    ID = models.IntegerField(primary_key=True, blank=True, null=False)
    ID_ESTACION = models.IntegerField()
    NOM_ESTACION = models.CharField(max_length=4000, blank=True, null=True)
    ESPECIE = models.CharField(max_length=2000, blank=True, null=True)
    ESPECIE_DES = models.CharField(max_length=4000, blank=True, null=True)
    ID_PROYECTO = models.IntegerField(blank=True, null=True)
    ID_METODOLOGIA = models.IntegerField()
    FECHA = models.DateField(null=True, blank=True)   
    TASA_MORTALIDAD = models.FloatField()   
    TASA_SUPERVIVENCIA= models.FloatField(blank=True, null=True) 
    class Meta:
        managed = False
        db_table = 'V_TASAS_RESTAURACION'
