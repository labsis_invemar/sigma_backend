from django.db.models.aggregates import Max, Min
from rest_framework.generics import ListAPIView
from .models import *
from .serializers import *
from rest_framework.response import Response
from rest_framework.views import APIView
from applications.estructura.models import *
from django.db.models import Sum

"""--------------------Ubicacion de parcela-------------------------------
class DatosDeSiembra(ListAPIView):
    serializer_class = DatosSiembraSerializer

    def get_queryset(self):
        par = self.kwargs['id_par']
       
        queryset =  V_datos_siembra.objects.all()
        
        return queryset
-----------------------------------------------------------------------"""

class DatosSiembra(APIView):
    #lista los datos de siembra
    def get(self, request, format=None):
        datos = V2021DatosSiembra.objects.using('manglar').all()
        serializer = siembraSerializer(datos, many=True)
        return Response(serializer.data)
"""
class DepDensidadList(ListAPIView):
    # queryset = VmEstructura.objects.values('departamento','densidad_estacion').distinct().filter(departamento__isnull=False).order_by('departamento').annotate(sum=Sum('densidad_estacion'))
    queryset = VmEstructura.objects.all()
    # raw('SELECT ID_ESTRUCTURA,DEPARTAMENTO FROM VM_ESTRUCTURA GROUP BY DEPARTAMENTO')
    serializer_class = DepartDensSerializer
    
# SELECT DEPARTAMENTO, SUM(DENSIDAD_ESTACION) FROM VM_ESTRUCTURA WHERE DEPARTAMENTO IS NOT NULL GROUP BY DEPARTAMENTO ORDER BY DEPARTAMENTO ASC 

class DensxDepList(ListAPIView):
    queryset = VmDensidadDep.objects.all()
    serializer_class = DensxDepSerializer
"""   
# -----------DATOS DE SIEMBRA ---------------------------
class restauracionData(APIView):
    #lista los datos de siembra
    def get(self, request, format=None):
        datos = V2021Restauracion.objects.using('manglar').all()
        serializer = restauracionDataSerializer(datos, many=True)
        return Response(serializer.data)
# ---------------departamentos-------------------------
class DepListRestauracion(APIView):
    def get(self, request, format=None):
        datos = V2021Restauracion.objects.using('manglar').values('departamento','cod_departamento').distinct().order_by('departamento')
        serializer = DepListRestauracionSerializer(datos, many=True)
        return Response(serializer.data)
    
class MunicipioDepartamento(ListAPIView):
    serializer_class = municipio_list_serializer
    def get_queryset(self):
        dep = self.kwargs['id_dep']  
        queryset = V2021Restauracion.objects.using('manglar').values('id_municipio','municipio').filter(cod_departamento=dep).distinct().order_by('municipio')
        return queryset

# ---------------proyectos por departamento---------------------------------
class ProjListRestauracion(ListAPIView):
    serializer_class = ProjListSerializer
    def get_queryset(self):
        mun = self.kwargs['id_mun']
        if mun =='todos':
            queryset = V2021Restauracion.objects.using('manglar').values('id_proyecto','proyecto').distinct().order_by('proyecto')
        else:
            queryset = V2021Restauracion.objects.using('manglar').values('id_proyecto','proyecto').filter(id_municipio=mun).distinct().order_by('proyecto')
        return queryset
    
#lista de estaciones
class EstList(ListAPIView):
    serializer_class = EstacionListSerializer
    
    def get_queryset(self):
        mun = self.kwargs['id_mun']
        proj = self.kwargs['id_proj']
        queryset = V2021Restauracion.objects.values('id_estacion','estacion').using('manglar').distinct().filter(id_municipio=mun).filter(id_proyecto=proj).order_by('estacion')
        return queryset
#lista de especies
class EspList(ListAPIView):
    serializer_class = EspecieListSerializer
    
    def get_queryset(self):
        est = self.kwargs['id_est']
        queryset = V2021Restauracion.objects.values('cod_especie','especie').using('manglar').distinct().filter(id_estacion=est).order_by('especie')
        return queryset    

"""
class DataEspecie(APIView):

    def get(self, request,id_est=None, cod_esp=None, *args, **kwargs):
        if cod_esp == "total":
            datos =  V2021Restauracion.objects.filter(id_estacion=id_est).order_by('ano','especie').values('ano','especie').using('manglar').distinct()
        else:
            datos =  V2021Restauracion.objects.filter(id_estacion=id_est).filter(cod_especie=cod_esp).order_by('ano','especie').values('ano','especie').using('manglar').distinct()
        
        serializer= VmRestauracionSerializer(datos, many=True,partial=True)
        
        # array=[]
        # for x in serializer.data:
        #     dict1={}
        #     for y in x:
        #         print(y)
        #         if x[y] is not None:
        #             dict1[y]=x[y]
        #     array.append(dict1)     
        return Response(serializer)
"""
class DataEspecie(ListAPIView):
    serializer_class= VmRestauracionSerializer
    
    def get_queryset(self):
        anio = self.kwargs['anio']
        id_est = self.kwargs['id_est']
        cod_esp = self.kwargs['cod_esp']
        if cod_esp == "total":
            datos =  V2021Restauracion.objects.filter(ano=anio).filter(id_estacion=id_est).order_by('ano','especie').values('ano','especie','siembraxesp').using('manglar').distinct()
        else:
            datos =  V2021Restauracion.objects.filter(ano=anio).filter(id_estacion=id_est).filter(cod_especie=cod_esp).order_by('ano','especie').values('ano','especie','siembraxesp').using('manglar').distinct()
        return datos    

class SiembraxDepartamentos(ListAPIView):
    serializer_class = SiembraxDepartamentosSerializer
    
    def get_queryset(self):
        
        queryset = V2021Restauracion.objects.values('departamento','porc_siembraxdep').distinct().using('manglar').order_by('departamento')
        return queryset 

class DataGeneralProyecto(ListAPIView):
    serializer_class= DataGeneralSerializer
    
    def get_queryset(self):
        anio = self.kwargs['anio']
        id_proy = self.kwargs['id_proy']
        id_est = self.kwargs['id_est']    
        datos =  V2021Restauracion.objects.using('manglar').filter(ano=anio).filter(id_proyecto=id_proy).filter(id_estacion=id_est).values('id_proyecto','proyecto','id_estacion','estacion','siembraxest','ESPXEST','ano','N_MONITOREOS','FECHA_INICIAL_MONITOREOS','ENTIDAD_FINANCIADORA','PLAZO_EJECUCION','FUENTE_FINANCIACION','AREA').distinct()
        # datos =  V2021Restauracion.objects.using('manglar').values('id_proyecto','proyecto','id_estacion','estacion','siembraxest','ESPXEST').distinct()
        # datos =  c.objects.filter(id_estacion=id_est).filter(cod_especie=cod_esp).order_by('ano','especie').values('ano','especie','sembrados').using('manglar').distinct()
        return datos 
    
class AniosRestauracion(ListAPIView):
    serializer_class= AniosRestauracionSerializer
    
    def get_queryset(self):
        id_est = self.kwargs['id_est']
        id_proy = self.kwargs['id_proj']
        anios = []
        datos =  V2021Restauracion.objects.using('manglar').values('ano').filter(id_proyecto=id_proy).filter(id_estacion=id_est).distinct()#.aggregate(total=Min('ano'))
        # for value in datos:
        #     value.append(1) # example usage
        #     anios.append(value) # example usage
        # datos = anios
        # datos =  c.objects.filter(id_estacion=id_est).filter(cod_especie=cod_esp).order_by('ano','especie').values('ano','especie','sembrados').using('manglar').distinct()
        return datos 

class MortalidadxEsp(ListAPIView):
    serializer_class= MortalidadxEspSerializer
    
    def get_queryset(self):
        id_est = self.kwargs['id_est']    
        # esp = self.kwargs['esp']
        anio = self.kwargs['anio']
        # select DISTINCT COD_ESPECIE, TASA_MORTALIDAD from V_2021_RESTAURACION where ID_ESTACION = 55345 and COD_ESPECIE = 'AG' and ANO =2021;.filter(especie=esp)
        datos =  V2021Restauracion.objects.using('manglar').filter(id_estacion=id_est).filter(ano=anio).values('especie','TASA_MORTALIDAD').distinct()
        # datos =  V2021Restauracion.objects.using('manglar').values('id_proyecto','proyecto','id_estacion','estacion','siembraxest','ESPXEST').distinct()
        # datos =  c.objects.filter(id_estacion=id_est).filter(cod_especie=cod_esp).order_by('ano','especie').values('ano','especie','sembrados').using('manglar').distinct()
        return datos 
    
class SupervivenciaxEsp(ListAPIView):
    serializer_class= SupervivenciaxEspSerializer
    
    def get_queryset(self):
        id_est = self.kwargs['id_est']    
        # esp = self.kwargs['esp']
        anio = self.kwargs['anio']
        # select DISTINCT COD_ESPECIE, TASA_MORTALIDAD from V_2021_RESTAURACION where ID_ESTACION = 55345 and COD_ESPECIE = 'AG' and ANO =2021;.filter(especie=esp)
        datos =  V2021Restauracion.objects.using('manglar').filter(id_estacion=id_est).filter(ano=anio).values('especie','TASA_SUPERVIVENCIA').distinct()
        # datos =  V2021Restauracion.objects.using('manglar').values('id_proyecto','proyecto','id_estacion','estacion','siembraxest','ESPXEST').distinct()
        # datos =  c.objects.filter(id_estacion=id_est).filter(cod_especie=cod_esp).order_by('ano','especie').values('ano','especie','sembrados').using('manglar').distinct()
        return datos    
class DensProp(ListAPIView):
    serializer_class= DensPropSerializer
    
    def get_queryset(self):
        id_est = self.kwargs['id_est']    
        #esp = self.kwargs['esp']
        anio = self.kwargs['anio']
       #select DISTINCT cod_especie, dens_plantulas from v_2021_restauracion where id_estacion = 52736 and cod_especie = 'AG' and ano = 2019
        datos =  V2021Restauracion.objects.using('manglar').filter(id_estacion=id_est).filter(ano=anio).values('especie','dens_propagulos').distinct()
       
        return datos
    
class DensPlant(ListAPIView):
    serializer_class= DensPlantSerializer
    
    def get_queryset(self):
        id_est = self.kwargs['id_est']    
        #esp = self.kwargs['esp']
        anio = self.kwargs['anio']
       #select DISTINCT cod_especie, dens_plantulas from v_2021_restauracion where id_estacion = 52736 and cod_especie = 'AG' and ano = 2019
        datos =  V2021Restauracion.objects.using('manglar').filter(id_estacion=id_est).filter(ano=anio).values('especie','dens_plantulas').distinct()
       
        return datos
    
class Tasa_mortalidad_restauracion(ListAPIView):
    serializer_class = TasaMortalidadRestauracion
    def get_queryset(self):
        id_est = self.kwargs['id_est']    
        datos =  V_tasas_restauracion.objects.using('manglar').filter(ID_ESTACION=id_est).values('FECHA','ESPECIE_DES','TASA_MORTALIDAD')
        return datos

class Tasa_supervivencia_restauracion(ListAPIView):
    serializer_class = TasaSupervivenciaRestauracion
    def get_queryset(self):
        id_est = self.kwargs['id_est']   
        
        datos =  V_tasas_restauracion.objects.using('manglar').filter(ID_ESTACION=id_est).values('FECHA','ESPECIE_DES','TASA_SUPERVIVENCIA')
        return datos

class area_reportada(ListAPIView):
    serializer_class = area_reportada_serializer
    def get_queryset(self):
        datos =  V2021Restauracion.objects.using('manglar').values('AREA').distinct()
        return datos
class areas_restauracion(ListAPIView):
    serializer_class = areas_restauracion_serializer
    def get_queryset(self):
        datos = V2021Restauracion.objects.using('manglar').values('id_estacion','estacion','AREA').distinct()
        return datos