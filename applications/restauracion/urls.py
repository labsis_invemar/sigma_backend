from django.urls import path
#from rest_framework.urlpatterns import format_suffix_patterns
from .views import *

urlpatterns =[
    path(r'api/contador-arboles/',DatosSiembra.as_view(),name='DatosSiembra'),
    path(r'api/restauracion/data-all/',restauracionData.as_view(),name='restauracionData'),
    # path(r'api/densidadDepartamento/',DepDensidadList.as_view(),name='DensidadDepartamento'),
    # path(r'api/densidadXDepartamento/',DensxDepList.as_view(),name='DensidadXDepartamento'),
    
    #Opciones del formulario
    path(r'api/restauracion/departamentos/',DepListRestauracion.as_view(),name='DepListRestauracion'),
    path(r'api/restauracion/municipios/<id_dep>/',MunicipioDepartamento.as_view(),name='MunListRestauracion'),
    path(r'api/restauracion/proyectos/<id_mun>/',ProjListRestauracion.as_view(),name='ProjListRestauracion'),
    path(r'api/restauracion/estaciones/<id_mun>/<id_proj>/',EstList.as_view(),name='EstList'),
    path(r'api/restauracion/anios/<id_est>/<id_proj>/',AniosRestauracion.as_view(),name='AniosRestauracion'),
    
    #not used
    path(r'api/restauracion/especies/<id_est>/',EspList.as_view(),name='EspList'),
    path(r'api/restauracion/restauracion-data/<anio>/<id_est>/<cod_esp>/',DataEspecie.as_view(),name='DataEspecie'), 
    
    #informacion general
    path(r'api/restauracion/siembra-departamentos/',SiembraxDepartamentos.as_view(),name='SiembraxDepartamentos'),
    path(r'api/restauracion/area-reportada/',area_reportada.as_view(),name='AreaReportada'),
    path(r'api/restauracion/data-general-proyecto/<anio>/<id_proy>/<id_est>/',DataGeneralProyecto.as_view(),name='DataGeneralProyecto'),
    ##path(r'api/restauracion/MortalidadxEsp/<id_est>/<anio>/',MortalidadxEsp.as_view(),name='MortalidadxEsp'),
    ##path(r'api/restauracion/SupervivenciaxEsp/<id_est>/<anio>/',SupervivenciaxEsp.as_view(),name='SupervivenciaxEsp'),
    
    #variables de respuesta
    path(r'api/restauracion/densidad-propagulos/<id_est>/<anio>/',DensProp.as_view(),name='DensProp'),
    path(r'api/restauracion/densidad-plantulas/<id_est>/<anio>/',DensPlant.as_view(),name='DensPlant'),
    path(r'api/restauracion/mortalidad-estacion/<id_est>/',Tasa_mortalidad_restauracion.as_view(),name='tasaMort'),
    path(r'api/restauracion/supervivencia-estacion/<id_est>/',Tasa_supervivencia_restauracion.as_view(),name='tasaSuperv'),
    
    path(r'api/restauracion/areas-restauracion/',areas_restauracion.as_view(),name='areas_restauracion'),
    #path(r'api/Restauracion-Data/',DataEspecie.as_view(),name='DataEspecie'), 
    #path('ecorregiones', views.Ecorregiones.as_view(), name='ecorregiones ')
]
#urlpatterns = format_suffix_patterns(urlpatterns)