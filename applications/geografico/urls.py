from django.contrib import admin
from django.urls import path
from .views import *


    
urlpatterns = [
    path(r'api/get-all-geografico',GeograficoList.as_view(),name='geografico_api_all'),
    path(r'api/get-ubicacion-parcela/<id_par>/',UbicacionParcela.as_view(),name='Ubicacion_Parcela'),
    path(r'api/get-ubicacion-parcela-estacion/<id_est>/',UbicacionParcelasEstacion.as_view(),name='Ubicacion_Parcela_estacion'),
]