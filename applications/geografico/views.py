from django.db.models.query import QuerySet
from django.shortcuts import render
from django.views.generic import TemplateView
from .models import *
from .serializers import *
from rest_framework.generics import(ListAPIView,)

# Create your views here.

class GeograficoList(ListAPIView):
    queryset = Clocalidadt.objects.filter(proyecto_loc=2239).filter(tipo_nodo__in=[35,40]).order_by('-path_lugar')
    serializer_class = ClocalidadtSerializer


#--------------------Ubicacion de parcela-------------------------------
class UbicacionParcela(ListAPIView):
    serializer_class = ParcelasGis

    def get_queryset(self):
        par = self.kwargs['id_par']
       
        queryset =  Clocalidadt.objects.filter(id_estacion=par)
        
        return queryset
#-----------------------------------------------------------------------


#--------------------Ubicación de parcelas por estacion----------------------
class UbicacionParcelasEstacion(ListAPIView):
    serializer_class = ParcelasGis

    def get_queryset(self):
        est = self.kwargs['id_est']

        queryset = Clocalidadt.objects.filter(nodo_padre=est).order_by('id_estacion')

        return queryset
#---------------------------------------------------------------------------