# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Clocalidadt(models.Model):
    id_estacion = models.IntegerField(primary_key=True, blank=True, null=False)
    secuencia_loc = models.IntegerField(blank=True, null=True)
    pais_loc = models.TextField(blank=True, null=True)  # This field type is a guess.
    proyecto_loc = models.FloatField(blank=True, null=True)
    nombre_alterno = models.TextField(blank=True, null=True)  # This field type is a guess.
    prefijo_cdg_est_loc = models.TextField(blank=True, null=True)  # This field type is a guess.
    codigo_estacion_loc = models.IntegerField(blank=True, null=True)
    nombre = models.TextField(blank=True, null=True,db_column="lugar")  # This field type is a guess.
    path_codigo = models.TextField(blank=True, null=True)  # This field type is a guess.
    path_lugar = models.TextField(blank=True, null=True)  # This field type is a guess.
    path_tipo_nodo = models.TextField(blank=True, null=True)  # This field type is a guess.
    vigente_loc = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_metodologia = models.IntegerField(blank=True, null=True)
    id_tipo_registro = models.BooleanField(blank=True, null=True)
    nodo_padre = models.IntegerField(blank=True, null=True)
    nodo_final = models.BooleanField(blank=True, null=True)
    tipo_nodo = models.IntegerField(blank=True, null=True)
    tipo_nodo_des = models.TextField(blank=True, null=True)  # This field type is a guess.
    fsistema = models.TextField(blank=True, null=True)  # This field type is a guess.
    factualizacion_vm = models.TextField(blank=True, null=True)  # This field type is a guess.
    activo = models.TextField(blank=True, null=True)  # This field type is a guess.
    ambiente_loc = models.FloatField(blank=True, null=True)
    area = models.FloatField(blank=True, null=True)
    area_ha = models.FloatField(blank=True, null=True)
    area_protegida = models.TextField(blank=True, null=True)  # This field type is a guess.
    azimut = models.FloatField(blank=True, null=True)
    barco_loc = models.TextField(blank=True, null=True)  # This field type is a guess.
    campana_loc = models.TextField(blank=True, null=True)  # This field type is a guess.
    car = models.TextField(blank=True, null=True)  # This field type is a guess.
    categoria_uni_manejo = models.TextField(blank=True, null=True)  # This field type is a guess.
    cdg_dpto = models.FloatField(blank=True, null=True)
    cuenca_hdg = models.TextField(blank=True, null=True)  # This field type is a guess.
    cuerpo_agua_loc = models.TextField(blank=True, null=True)  # This field type is a guess.
    dato_temporal = models.TextField(blank=True, null=True)  # This field type is a guess.
    desc_area = models.TextField(blank=True, null=True)  # This field type is a guess.
    descripcion_estacion_loc = models.TextField(blank=True, null=True)  # This field type is a guess.
    descripcion_nodo = models.TextField(blank=True, null=True)  # This field type is a guess.
    distanciacosta_loc = models.FloatField(blank=True, null=True)
    dummy = models.FloatField(blank=True, null=True)
    ecorregion_loc = models.FloatField(blank=True, null=True)
    entidad_loc = models.TextField(blank=True, null=True)  # This field type is a guess.
    error_d = models.TextField(blank=True, null=True)  # This field type is a guess.
    fecha_inicial_mtortuga = models.TextField(blank=True, null=True)  # This field type is a guess.
    fecha_final_mtortuga = models.TextField(blank=True, null=True)  # This field type is a guess.
    fecha_instalacion_par = models.TextField(blank=True, null=True)  # This field type is a guess.
    fechacreacion = models.TextField(blank=True, null=True)  # This field type is a guess.
    fecha_sistema = models.TextField(blank=True, null=True)  # This field type is a guess.
    forma_parcela = models.FloatField(blank=True, null=True)
    geomorfologia = models.TextField(blank=True, null=True)  # This field type is a guess.
    hondoagua_loc = models.FloatField(blank=True, null=True)
    latitudfin_loc = models.FloatField(blank=True, null=True)
    latitud = models.FloatField(blank=True, null=True,db_column="latitudinicio_loc")
    longitudfin_loc = models.FloatField(blank=True, null=True)
    longitud = models.FloatField(blank=True, null=True,db_column="longitudinicio_loc")
    marrio_loc = models.TextField(blank=True, null=True)  # This field type is a guess.
    notas_loc = models.TextField(blank=True, null=True)  # This field type is a guess.
    observaciones_loc = models.TextField(blank=True, null=True)  # This field type is a guess.
    prof_max_loc = models.FloatField(blank=True, null=True)
    prof_min_loc = models.FloatField(blank=True, null=True)
    proyecto_dueno = models.TextField(blank=True, null=True)  # This field type is a guess.
    region = models.TextField(blank=True, null=True)  # This field type is a guess.
    salodulce_loc = models.TextField(blank=True, null=True)  # This field type is a guess.
    secuencia_char = models.TextField(blank=True, null=True)  # This field type is a guess.
    sustrato_loc = models.FloatField(blank=True, null=True)
    tipo_componente = models.FloatField(blank=True, null=True)
    tipo_fisiografico = models.FloatField(blank=True, null=True)
    toponimia_loc = models.FloatField(blank=True, null=True)
    uac = models.FloatField(blank=True, null=True)
    wkt_area_muestreo = models.TextField(blank=True, null=True)  # This field type is a guess.
    zona = models.TextField(blank=True, null=True)  # This field type is a guess.
    zona_protegida = models.TextField(blank=True, null=True)  # This field type is a guess.
    tipocosta_loc = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Clocalidadt'


