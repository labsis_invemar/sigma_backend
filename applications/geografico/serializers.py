from rest_framework import serializers
from .models import *
from rest_framework_gis.serializers import GeoFeatureModelSerializer,GeometrySerializerMethodField
from django.contrib.gis.geos import Point


class ClocalidadtSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clocalidadt
        fields =('tipo_nodo_des','id_estacion','nombre','nodo_padre','fecha_instalacion_par','latitud','longitud')



class ParcelasGis(GeoFeatureModelSerializer):
    pnt = GeometrySerializerMethodField()
    class Meta:
        model = Clocalidadt
        geo_field = 'pnt'
        fields=('nombre','latitud','longitud')
    
    def get_pnt(self, obj):
       return Point(float(obj.longitud),float(obj.latitud))