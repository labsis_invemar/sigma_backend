FROM python:3.6

RUN mkdir /opt/oracle && mkdir /code && cd /opt/oracle 
RUN apt-get update && apt-get install apt-utils -y && apt-get install wget -y && apt-get install unzip -y && apt-get install gdal-bin -y && apt-get install libaio1 -y && apt-get install gunicorn -y
RUN wget https://download.oracle.com/otn_software/linux/instantclient/19600/instantclient-basic-linux.x64-19.6.0.0.0dbru.zip && unzip instantclient-basic-linux.x64-19.6.0.0.0dbru.zip && rm instantclient-basic-linux.x64-19.6.0.0.0dbru.zip && rm -rf /var/lib/apt/lists/*
RUN sh -c "echo /instantclient_19_6 > /etc/ld.so.conf.d/oracle-instantclient.conf" && ldconfig && export LD_LIBRARY_PATH=/instantclient_19_6:$LD_LIBRARY_PATH 
ENV PYTHONUNBUFFERED 1
WORKDIR /code
COPY . /code/
RUN pip install -r requirements.txt && pip install gunicorn[eventlet]
#RUN python manage.py migrate --settings=sigma.settings.prod
CMD ["gunicorn", "-c", "config/gunicorn/conf.py", "--bind", ":8300", "--chdir", "sigma", "sigma.wsgi:application"]

#----------------------------run as simple container --------------------------------
#FORCE STOP (-F) AND REMOVING THE CONTAINER 
    # docker rm -f geovisor_back

#REMOVING THE LAST IMG
    # docker rmi geov_back

#BUILDING THE IMG
    # docker build -t geov_back .

#RUNING THE CONTAINER (:cached)
    # docker run -d -v "%cd%\applications:/code/applications:cached" --name geovisor_back -p 8300:8300 geov_back

#EXEC ALL IN ONE COMMAND POWERSHELL
    # (docker rm -f geovisor_back) -and (docker rmi geov_back) -and (docker build -t geov_back .) -and  (docker run --name geovisor_back -dp 8300:8300 geov_back) 
    # docker ps

#EXEC ALL IN ONE COMMAND CMD (BETTER WAY)
    # (docker rm -f geovisor_back) & (docker rmi geov_back) & (docker build -t geov_back .) &&  (docker run -d -v "%cd%\applications:/code/applications:cached" --name geovisor_back -p 8300:8300 geov_back) && (docker ps)
