from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*',]



# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
   'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },
    'manglar': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'sci',
        'USER': 'MANGLAR',
        'PASSWORD': 'laguncularia',
        'HOST': '192.168.3.70',
        'PORT': '1521',
    },

    'geografico': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'sci',
        'USER': 'geograficos',
        'PASSWORD': 'estaciones',
        'HOST': '192.168.3.70',
        'PORT': '1521',
    },
}


#Para migrar tablas y vistas
#python manage.py inspectdb vm_caracterizacion --database geografico > applications/geografico/models.py

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = '/code/static/'