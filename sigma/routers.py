class DatosRouter:

    apps = {
        'estructura',
        'fisicoquimico',
        'regeneracion',
        'base',
    }
    geo = {
        'geografico',
    }

  

    def db_for_read(self, model, **hints):
        if model._meta.app_label in self.apps:
            return 'manglar'
        if model._meta.app_label in self.geo:
            return 'geografico'
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label in self.apps:
            return 'manglar'
        if model._meta.app_label in self.geo:
            return 'geografico'
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label in self.apps:
            return 'manglar'
        if app_label in self.geo:
            return 'geografico'
        return None

    def allow_relation(self, elem1, elem2, **hints):
        if (elem1._meta.app_label in self.apps and elem2._meta.app_label in self.apps) or (elem1._meta.app_label in self.geo and elem2._meta.app_label in self.geo):
            return True
        return None